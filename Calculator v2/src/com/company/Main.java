package com.company;

import com.company.Operations.DivisionOper;
import com.company.Operations.MinusOper;
import com.company.Operations.MultiplicationOper;
import com.company.Operations.PlusOper;

import java.lang.reflect.Array;
import java.util.*;

public class Main {

    public static void main(String[] args) {


        Operation plusOp = new PlusOper();
        Operation minusOp = new MinusOper();
        Operation multiplicationOp = new MultiplicationOper();
        Operation divisionOp = new DivisionOper();

        List<Operation> operations = new ArrayList<>();
        operations.add(plusOp);
        operations.add(minusOp);
        operations.add(multiplicationOp);
        operations.add(divisionOp);


        Validator validator = new SimpleValidator();
        validator.setOperations(operations);

        String expression = "1-1+3*4/5*5+2";
        validator.validate(expression);


    }
}

