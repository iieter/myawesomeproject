package com.company;

import java.util.List;

public class SimpleCalc extends Calculator {

    public SimpleCalc(
            List<Operation> operations,
            Validator validator) {
        super(operations, validator);
    }

    @Override
    protected String childCalculate(String expression) {
        String result = "";
        for (Operation operation : operations) {
            result = operation.exec(expression);
        }
        return result;
    }
}

