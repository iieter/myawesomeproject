package com.company;


public abstract class Operation implements Comparable<Operation> {
    private String name = "";
    protected Integer priority = 0;

    public Operation(String name, int priority) {
        this.name = name;
        this.priority = priority;
    }

    @Override
    public int compareTo(Operation o) {
        return o.priority.compareTo(this.priority);
    }

    public int getPriority() {
        return priority;
    }

    public Operation(String operatorName) {
        this.name = operatorName;
    }

    public String getName() {
        return name;
    }

    public String trimExpression(String expression) {
        int index = expression.indexOf(this.getName());
        String op1 = expression.substring(index - 1, index);
        String op2 = expression.substring(index + 1, index + 2);
        if (op1 != null && op2 != null
                && isNumber(op1) && isNumber(op2)) {
            return new StringBuilder(expression).replace(index - 1, index + 2, "1").toString();
        }
        return null;
    }

    public abstract String exec(String expression);

    private boolean isNumber(String number) {
        try {
            Double.parseDouble(number);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
}
