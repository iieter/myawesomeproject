package com.company;

import java.util.Collections;
import java.util.List;


public class SimpleValidator implements Validator {
    private List<Operation> operations;


    @Override
    public void validate(String expression) {
        String tmpExp = expression;
        Collections.sort(operations);
        for (Operation operation :
                operations) {
            while (!tmpExp.equals("1") &&
                    tmpExp.contains(operation.getName())) {
                tmpExp = operation.trimExpression(tmpExp);
                System.out.println(tmpExp);
            }
        }
    }


    @Override
    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

}

