package com.company;

import java.util.Collections;
import java.util.List;

public abstract class Calculator {

    private Validator validator;

    protected List<Operation> operations;

    public Calculator(List<Operation> operations, Validator validator) {
        if (operations == null && operations.isEmpty()) {
            throw new IllegalArgumentException("");
        }
        Collections.sort(operations);
        this.validator = validator;
        this.validator.setOperations(operations);
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
        this.validator.setOperations(this.operations);
    }

    public String calculate(String expression) {
        validator.validate(expression);
        return childCalculate(expression);
    }

    protected abstract String childCalculate(String expression);
}
