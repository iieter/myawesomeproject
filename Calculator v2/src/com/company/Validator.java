package com.company;

import java.util.List;

public interface Validator {


    void validate(String expression);

    void setOperations(List<Operation> operations);
}
